package services

import (
	"context"
	"log"
	"strconv"

	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
)

type ProfileService interface {
	GetProfiles(ctx context.Context, hotelIds []string, locale string) ([]HotelProfile, error)
}

type ProfileServiceImpl struct {
	profileCache components.Cache
	profileDB    components.NoSQLDatabase
}

func initProfileDB(db components.NoSQLDatabase) error {
	c := db.GetDatabase("profile-db").GetCollection("hotels")
	err := c.InsertOne(&HotelProfile{
		"1",
		"Clift Hotel",
		"(415) 775-4700",
		"A 6-minute walk from Union Square and 4 minutes from a Muni Metro station, this luxury hotel designed by Philippe Starck features an artsy furniture collection in the lobby, including work by Salvador Dali.",
		Address{
			"495",
			"Geary St",
			"San Francisco",
			"CA",
			"United States",
			"94102",
			37.7867,
			-122.4112}})
	if err != nil {
		return err
	}

	err = c.InsertOne(&HotelProfile{
		"2",
		"W San Francisco",
		"(415) 777-5300",
		"Less than a block from the Yerba Buena Center for the Arts, this trendy hotel is a 12-minute walk from Union Square.",
		Address{
			"181",
			"3rd St",
			"San Francisco",
			"CA",
			"United States",
			"94103",
			37.7854,
			-122.4005}})
	if err != nil {
		return err
	}

	err = c.InsertOne(&HotelProfile{
		"3",
		"Hotel Zetta",
		"(415) 543-8555",
		"A 3-minute walk from the Powell Street cable-car turnaround and BART rail station, this hip hotel 9 minutes from Union Square combines high-tech lodging with artsy touches.",
		Address{
			"55",
			"5th St",
			"San Francisco",
			"CA",
			"United States",
			"94103",
			37.7834,
			-122.4071}})
	if err != nil {
		return err
	}

	err = c.InsertOne(&HotelProfile{
		"4",
		"Hotel Vitale",
		"(415) 278-3700",
		"This waterfront hotel with Bay Bridge views is 3 blocks from the Financial District and a 4-minute walk from the Ferry Building.",
		Address{
			"8",
			"Mission St",
			"San Francisco",
			"CA",
			"United States",
			"94105",
			37.7936,
			-122.3930}})
	if err != nil {
		return err
	}

	err = c.InsertOne(&HotelProfile{
		"5",
		"Phoenix Hotel",
		"(415) 776-1380",
		"Located in the Tenderloin neighborhood, a 10-minute walk from a BART rail station, this retro motor lodge has hosted many rock musicians and other celebrities since the 1950s. It’s a 4-minute walk from the historic Great American Music Hall nightclub.",
		Address{
			"601",
			"Eddy St",
			"San Francisco",
			"CA",
			"United States",
			"94109",
			37.7831,
			-122.4181}})
	if err != nil {
		return err
	}

	err = c.InsertOne(&HotelProfile{
		"6",
		"St. Regis San Francisco",
		"(415) 284-4000",
		"St. Regis Museum Tower is a 42-story, 484 ft skyscraper in the South of Market district of San Francisco, California, adjacent to Yerba Buena Gardens, Moscone Center, PacBell Building and the San Francisco Museum of Modern Art.",
		Address{
			"125",
			"3rd St",
			"San Francisco",
			"CA",
			"United States",
			"94109",
			37.7863,
			-122.4015}})
	if err != nil {
		return err
	}

	// add up to 80 hotels
	for i := 7; i <= 80; i++ {
		hotel_id := strconv.Itoa(i)
		phone_num := "(415) 284-40" + hotel_id
		lat := 37.7835 + float64(i)/500.0*3
		lon := -122.41 + float64(i)/500.0*4
		err = c.InsertOne(&HotelProfile{
			hotel_id,
			"St. Regis San Francisco",
			phone_num,
			"St. Regis Museum Tower is a 42-story, 484 ft skyscraper in the South of Market district of San Francisco, California, adjacent to Yerba Buena Gardens, Moscone Center, PacBell Building and the San Francisco Museum of Modern Art.",
			Address{
				"125",
				"3rd St",
				"San Francisco",
				"CA",
				"United States",
				"94109",
				lat,
				lon}})
		if err != nil {
			return err
		}
	}

	return nil
}

func NewProfileServiceImpl(profileCache components.Cache, profileDB components.NoSQLDatabase) *ProfileServiceImpl {
	err := initProfileDB(profileDB)
	if err != nil {
		log.Fatal(err)
	}
	return &ProfileServiceImpl{profileCache: profileCache, profileDB: profileDB}
}

func (p *ProfileServiceImpl) GetProfiles(ctx context.Context, hotelIds []string, locale string) ([]HotelProfile, error) {
	var profiles []HotelProfile

	for _, hid := range hotelIds {
		var profile HotelProfile
		err := p.profileCache.Get(hid, &profile)
		if err != nil {
			// Check Database
			collection := p.profileDB.GetDatabase("profile-db").GetCollection("hotels")
			query := `{"Id":"` + hid + `"}`
			res, err := collection.FindOne(query)
			if err != nil {
				return profiles, err
			}
			res.Decode(&profile)
			err = p.profileCache.Put(hid, profile)
		}
		profiles = append(profiles, profile)
	}

	return profiles, nil
}
