package services

import (
	"context"
	"log"
	"strconv"

	"github.com/hailocab/go-geoindex"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
)

type GeoService interface {
	Nearby(ctx context.Context, lat float64, lon float64) ([]string, error)
}

type GeoServiceImpl struct {
	geoDB components.NoSQLDatabase
	index *geoindex.ClusteringIndex
}

func initGeoDB(db components.NoSQLDatabase) error {
	c := db.GetDatabase("geo-db").GetCollection("geo")
	err := c.InsertOne(&Point{"1", 37.7867, -122.4112})
	if err != nil {
		return err
	}

	err = c.InsertOne(&Point{"2", 37.7854, -122.4005})
	if err != nil {
		return err
	}

	err = c.InsertOne(&Point{"3", 37.7854, -122.4071})
	if err != nil {
		return err
	}

	err = c.InsertOne(&Point{"4", 37.7936, -122.3930})
	if err != nil {
		return err
	}

	err = c.InsertOne(&Point{"5", 37.7831, -122.4181})
	if err != nil {
		return err
	}

	err = c.InsertOne(&Point{"6", 37.7863, -122.4015})
	if err != nil {
		return err
	}

	// add up to 80 hotels
	for i := 7; i <= 80; i++ {
		hotel_id := strconv.Itoa(i)
		lat := 37.7835 + float64(i)/500.0*3
		lon := -122.41 + float64(i)/500.0*4
		err = c.InsertOne(&Point{hotel_id, lat, lon})
		if err != nil {
			return err
		}
	}

	return nil
}

func NewGeoServiceImpl(geoDB components.NoSQLDatabase) *GeoServiceImpl {
	err := initGeoDB(geoDB)
	if err != nil {
		log.Fatal(err)
	}
	service := &GeoServiceImpl{geoDB: geoDB}
	service.newGeoIndex(context.Background())
	return service
}

const (
	MAXSEARCHRESULTS = 5
	MAXSEARCHRADIUS  = 10
)

func (g *GeoServiceImpl) newGeoIndex(ctx context.Context) error {
	collection := g.geoDB.GetDatabase("geo-db").GetCollection("geo")
	var points []Point
	res, err := collection.FindMany("")
	if err != nil {
		return err
	}
	res.All(&points)
	g.index = geoindex.NewClusteringIndex()
	for _, point := range points {
		g.index.Add(point)
	}
	return nil
}

func (g *GeoServiceImpl) getNearbyPoints(lat float64, lon float64) []geoindex.Point {
	center := &Point{Pid: "", Plat: lat, Plon: lon}

	return g.index.KNearest(center, MAXSEARCHRESULTS, geoindex.Km(MAXSEARCHRADIUS), func(p geoindex.Point) bool { return true })
}

func (g *GeoServiceImpl) Nearby(ctx context.Context, lat float64, lon float64) ([]string, error) {
	points := g.getNearbyPoints(lat, lon)
	var hotelIds []string
	for _, p := range points {
		hotelIds = append(hotelIds, p.Id())
	}

	return hotelIds, nil
}
