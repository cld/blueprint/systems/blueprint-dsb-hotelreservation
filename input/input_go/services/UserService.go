package services

import (
	"context"
	"crypto/sha256"
	"fmt"
	"log"
	"strconv"

	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
)

type UserService interface {
	CheckUser(ctx context.Context, username string, password string) (bool, error)
}

type UserServiceImpl struct {
	users map[string]string
	userDB components.NoSQLDatabase
}

func initUserDB(userDB components.NoSQLDatabase) error {
	c := userDB.GetDatabase("user-db").GetCollection("user")
	for i := 0; i <= 500; i++ {
		suffix := strconv.Itoa(i)
		user_name := "Cornell_" + suffix
		password := ""
		for j := 0; j < 10; j++ {
			password += suffix
		}

		sum := sha256.Sum256([]byte(password))
		pass := fmt.Sprintf("%x", sum)
		err := c.InsertOne(&User{user_name, pass})
		if err != nil {
			return err
		}
	}
	return nil
}

func NewUserServiceImpl(userDB components.NoSQLDatabase) *UserServiceImpl {
	u := &UserServiceImpl{userDB: userDB, users: make(map[string]string)}
	err := initUserDB(userDB)
	if err != nil {
		log.Fatal(err)
	}
	err = u.LoadUsers(context.Background())
	if err != nil {
		log.Fatal(err)
	}
	return u
}

func (u *UserServiceImpl) LoadUsers(ctx context.Context) error {
	collection := u.userDB.GetDatabase("user-db").GetCollection("user")

	var users []User
	result, err := collection.FindMany("")
	if err != nil {
		return err
	}
	result.All(&users)

	for _, user := range users {
		u.users[user.Username] = user.Password
	}
	return nil
}

func (u *UserServiceImpl) CheckUser(ctx context.Context, username string, password string) (bool, error) {
	sum := sha256.Sum256([]byte(password))
	pass := fmt.Sprintf("%x", sum)

	result := false
	if true_pass, found := u.users[username]; found {
		result = pass == true_pass
	}
	return result, nil
}
