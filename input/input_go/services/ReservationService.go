package services

import (
	"context"
	"log"
	"strconv"
	"time"

	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"

	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/debug"
)

type ReservationService interface {
	MakeReservation(ctx context.Context, customerName string, hotelIds []string, inDate string, outDate string, roomNumber int64) ([]string, error)
	CheckAvailability(ctx context.Context, customerName string, hotelIDs []string, inDate string, outDate string, roomNumber int64) ([]string, error)
}

type ReservationServiceImpl struct {
	reserveCache components.Cache
	reserveDB    components.NoSQLDatabase
	CacheHits    int64
	NumRequests  int64
}

func initReservationDB(db components.NoSQLDatabase) error {

	c := db.GetDatabase("reservation-db").GetCollection("reservation")
	err := c.InsertOne(&Reservation{"4", "Alice", "2015-04-09", "2015-04-10", 1})
	if err != nil {
		return err
	}

	c = db.GetDatabase("reservation-db").GetCollection("number")
	err = c.InsertOne(&HotelNumber{"1", 200})
	if err != nil {
		return err
	}

	err = c.InsertOne(&HotelNumber{"2", 200})
	if err != nil {
		return err
	}

	err = c.InsertOne(&HotelNumber{"3", 200})
	if err != nil {
		return err
	}

	err = c.InsertOne(&HotelNumber{"4", 200})
	if err != nil {
		return err
	}

	err = c.InsertOne(&HotelNumber{"5", 200})
	if err != nil {
		return err
	}

	err = c.InsertOne(&HotelNumber{"6", 200})
	if err != nil {
		return err
	}

	for i := 7; i <= 80; i++ {
		hotel_id := strconv.Itoa(i)
		var room_num int64
		room_num = 200
		if i%3 == 1 {
			room_num = 300
		} else if i%3 == 2 {
			room_num = 250
		}
		err = c.InsertOne(&HotelNumber{hotel_id, room_num})
		if err != nil {
			return err
		}
	}

	return nil
}

func metricFunc(r *ReservationServiceImpl) {
	ticker := time.NewTicker(1 * time.Second)
	for {
		select {
		case <-ticker.C:
			debug.ReportMetric("CacheHitRate", float64(r.CacheHits)/float64(r.NumRequests))
			r.CacheHits = 0
			r.NumRequests = 0
		}
	}
}

func NewReservationServiceImpl(reserveCache components.Cache, reserveDB components.NoSQLDatabase) *ReservationServiceImpl {
	err := initReservationDB(reserveDB)
	if err != nil {
		log.Fatal(err)
	}
	r := &ReservationServiceImpl{reserveCache: reserveCache, reserveDB: reserveDB}
	return r
}

func (r *ReservationServiceImpl) MakeReservation(ctx context.Context, customerName string, hotelIds []string, inDate string, outDate string, roomNumber int64) ([]string, error) {
	reservation_collection := r.reserveDB.GetDatabase("reservation-db").GetCollection("reservation")
	hnumber_collection := r.reserveDB.GetDatabase("reservation-db").GetCollection("number")
	newInDate, _ := time.Parse(time.RFC3339, inDate+"T12:00:00+00:00")
	newOutDate, _ := time.Parse(time.RFC3339, outDate+"T12:00:00+00:00")
	hotelId := hotelIds[0]
	indate := newInDate.String()[0:10]

	reservation_update_map := make(map[string]int64)
	for newInDate.Before(newOutDate) {
		newInDate = newInDate.AddDate(0, 0, 1)
		outdate := newInDate.String()[0:10]

		key := hotelId + "_" + newInDate.String()[0:10] + "_" + outdate
		var room_number int64
		err := r.reserveCache.Get(key, &room_number)
		if err != nil {
			var reservations []Reservation
			query := `{"HotelId":"` + hotelId + `", "InDate":"` + indate + `", "OutDate":"` + outdate + `"}`
			res, err := reservation_collection.FindMany(query)
			if err != nil {
				return []string{}, err
			}
			res.All(&reservations)
			for _, reservation := range reservations {
				room_number += reservation.Number
			}
		}
		reservation_update_map[key] = room_number + roomNumber

		// Check capacity
		cap_key := hotelId + "_cap"
		var hotelNumber HotelNumber
		var capacity int64
		err = r.reserveCache.Get(cap_key, &capacity)
		if err != nil {
			query := `{"HotelId":"` + hotelId + `"}`
			res, err := hnumber_collection.FindOne(query)
			if err != nil {
				return []string{}, nil
			}
			res.Decode(&hotelNumber)
			capacity = hotelNumber.Number
			err = r.reserveCache.Put(cap_key, capacity)
			if err != nil {
				return []string{}, nil
			}
		}
		if room_number+roomNumber > capacity {
			return []string{}, nil
		}
		indate = outdate
	}

	newInDate, _ = time.Parse(time.RFC3339, inDate+"T12:00:00+00:00")
	indate = newInDate.String()[0:10]

	for newInDate.Before(newOutDate) {
		newInDate = newInDate.AddDate(0, 0, 1)
		outdate := newInDate.String()[0:10]
		reservation := Reservation{HotelId: hotelId, CustomerName: customerName, InDate: indate, OutDate: outdate, Number: roomNumber}
		err := reservation_collection.InsertOne(reservation)
		if err != nil {
			return []string{}, err
		}
	}

	for key, val := range reservation_update_map {
		err := r.reserveCache.Put(key, val)
		if err != nil {
			return []string{}, err
		}
	}

	return []string{hotelId}, nil
}

func (r *ReservationServiceImpl) CheckAvailability(ctx context.Context, customerName string, hotelIds []string, inDate string, outDate string, roomNumber int64) ([]string, error) {
	reservation_collection := r.reserveDB.GetDatabase("reservation-db").GetCollection("reservation")
	hnumber_collection := r.reserveDB.GetDatabase("reservation-db").GetCollection("number")

	var available_hotels []string

	for _, hotelId := range hotelIds {
		newInDate, _ := time.Parse(time.RFC3339, inDate+"T12:00:00+00:00")
		newOutDate, _ := time.Parse(time.RFC3339, outDate+"T12:00:00+00:00")
		indate := newInDate.String()[0:10]

		for newInDate.Before(newOutDate) {
			newInDate = newInDate.AddDate(0, 0, 1)
			outdate := newInDate.String()[0:10]
			key := hotelId + "_" + newInDate.String()[0:10] + "_" + outdate
			r.NumRequests += 1
			var count int64
			err := r.reserveCache.Get(key, &count)
			if err != nil {
				// Check Database
				var reservations []Reservation
				query := `{"HotelId":"` + hotelId + `", "InDate":"` + indate + `", "OutDate":"` + outdate + `"}`
				res, err := reservation_collection.FindMany(query)
				if err != nil {
					return []string{}, err
				}
				res.All(&reservations)
				for _, reservation := range reservations {
					count += reservation.Number
				}
				err = r.reserveCache.Put(key, count)
				if err != nil {
					return []string{}, err
				}
			} else {
				r.CacheHits += 1
			}

			// Check capacity
			cap_key := hotelId + "_cap"
			var capacity int64
			var hotelNumber HotelNumber
			err = r.reserveCache.Get(cap_key, &capacity)
			r.NumRequests += 1
			if err != nil {
				query := `{"HotelId":"` + hotelId + `"}`
				res, err := hnumber_collection.FindOne(query)
				if err != nil {
					return []string{}, err
				}
				res.Decode(&hotelNumber)
				capacity = hotelNumber.Number
				err = r.reserveCache.Put(cap_key, capacity)
				if err != nil {
					return []string{}, err
				}
			} else {
				r.CacheHits += 1
			}
			if count+roomNumber > capacity {
				break
			}

			if newInDate.Equal(newOutDate) {
				available_hotels = append(available_hotels, hotelId)
			}
		}
	}

	return available_hotels, nil
}
