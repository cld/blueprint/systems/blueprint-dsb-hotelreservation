module services

go 1.18

require github.com/hailocab/go-geoindex v0.0.0-20160127134810-64631bfe9711

require github.com/gogo/protobuf v1.3.2 // indirect

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/tracingplane/tracingplane-go v0.0.0-20171025152126-8c4e6f79b148 // indirect
	gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler v0.0.1
	go.opentelemetry.io/otel v1.6.1 // indirect
	go.opentelemetry.io/otel/trace v1.6.1 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
