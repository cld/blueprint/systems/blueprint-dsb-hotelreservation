# blueprint-dsb-hotelreservation

Legacy Blueprint version of the Hotel Reservation application from the DeathStarBench microservice benchmark suite.

Visit https://github.com/Blueprint-uServices for the actively maintained version of Blueprint.
