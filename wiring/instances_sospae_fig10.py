rpc_server : Modifier = RPCServer(framework="grpc", timeout="500ms")
web_server : Modifier = WebServer(framework="default", timeout="100ms")
default_deployer : Modifier = Deployer(framework="docker", deployer_opts={'public_ports': False})
normal_deployer : Modifier = Deployer(framework="docker", deployer_opts={'public_ports': True})
default_client_opts : Modifier = Retry(max_retries=10)
cb_opts : Modifier = CircuitBreaker(interval="1s")
jaegerTracer : Tracer = JaegerTracer().WithServer(normal_deployer)

jaegerTracerModifier: Callable[str, Modifier] = lambda x : TracerModifier(tracer=jaegerTracer, service_name=x)

server_modifiers : Callable[str, List[Modifier]] = lambda x : [rpc_server, default_deployer]
client_modifiers : List[Modifier] = [default_client_opts, cb_opts]
web_server_modifiers : Callable[str, List[Modifier]] = lambda x : [normal_deployer, web_server]
queue_modifiers : Callable[str, List[Modifier]] = lambda x : [default_deployer]

userDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
recomdDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
reservDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
geoDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
rateDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
profileDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)

reservCache : Cache = Memcached().WithServer(default_deployer)
rateCache : Cache = Memcached().WithServer(default_deployer)
profileCache : Cache = Memcached().WithServer(default_deployer)

userService : UserService = UserServiceImpl(userDatabase=userDatabase).WithServer(server_modifiers("userService")).WithClient(client_modifiers)

recomdService : RecommendationService = RecommendationServiceImpl(recomdDatabase=recomdDatabase).WithServer(server_modifiers("recomdService")).WithClient(client_modifiers)

reservService : ReservationService = ReservationServiceImpl(reservCache=reservCache, reservDatabase=reservDatabase).WithServer(server_modifiers("reservService")).WithClient(client_modifiers)

geoService : GeoService = GeoServiceImpl(geoDB=geoDatabase).WithServer(server_modifiers("geoService")).WithClient(client_modifiers)

rateService : RateService = RateServiceImpl(rateCache=rateCache,rateDatabase=rateDatabase).WithServer(server_modifiers("rateService")).WithClient(client_modifiers)

profileService : ProfileService = ProfileServiceImpl(profileCache=profileCache,profileDatabase=profileDatabase).WithServer(server_modifiers("profileService")).WithClient(client_modifiers)

searchService : SearchService = SearchServiceImpl(geoClient=geoService, rateClient=rateService).WithServer(server_modifiers("searchService")).WithClient(client_modifiers)

frontendService : FrontEndService = FrontEndServiceImpl(searchService=searchService,profileService=profileService,recommendationService=recomdService,userService=userService,reservationService=reservService).WithServer(web_server_modifiers("frontendService"))
