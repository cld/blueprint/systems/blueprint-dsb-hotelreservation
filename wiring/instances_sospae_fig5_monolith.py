web_server : Modifier = WebServer(framework="default")
default_deployer : Modifier = Deployer(framework="docker", deployer_opts={'public_ports': False})
normal_deployer : Modifier = Deployer(framework="docker", deployer_opts={'public_ports': True})
jaegerTracer : Tracer = JaegerTracer().WithServer(normal_deployer)

jaegerTracerModifier: Callable[str, Modifier] = lambda x : TracerModifier(tracer=jaegerTracer, service_name=x)

server_modifiers : Callable[str, List[Modifier]] = lambda x : [rpc_server, default_deployer]
client_modifiers : List[Modifier] = []
web_server_modifiers : Callable[str, List[Modifier]] = lambda x : [normal_deployer, web_server]
queue_modifiers : Callable[str, List[Modifier]] = lambda x : [default_deployer]

userDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
recomdDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
reservDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
geoDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
rateDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
profileDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)

reservCache : Cache = Memcached().WithServer(default_deployer)
rateCache : Cache = Memcached().WithServer(default_deployer)
profileCache : Cache = Memcached().WithServer(default_deployer)

userService : UserService = UserServiceImpl(userDatabase=userDatabase)

recomdService : RecommendationService = RecommendationServiceImpl(recomdDatabase=recomdDatabase)

reservService : ReservationService = ReservationServiceImpl(reservCache=reservCache, reservDatabase=reservDatabase)

geoService : GeoService = GeoServiceImpl(geoDB=geoDatabase)

rateService : RateService = RateServiceImpl(rateCache=rateCache,rateDatabase=rateDatabase)

profileService : ProfileService = ProfileServiceImpl(profileCache=profileCache,profileDatabase=profileDatabase)

searchService : SearchService = SearchServiceImpl(geoClient=geoService, rateClient=rateService)

frontendService : FrontEndService = FrontEndServiceImpl(searchService=searchService,profileService=profileService,recommendationService=recomdService,userService=userService,reservationService=reservService).WithServer(web_server_modifiers("frontendService"))

monolith_process : Process = Process(services=[userService, recomdService, reservService, geoService, rateService, profileService, searchService, frontendService])
